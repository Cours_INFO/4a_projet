# **Follow-up sheet - Team 17**

## **Team's members**:
* BONFILS Antoine
* FERRARI Julien
* CATTEAU Pierrick

## **WEEK 1 - January 16th**

* Start to install the environment + fix installation problem
* Reading of all the README of the git repository
* Start to learn the technologies (React, Docker, yarn) and programming languages (TypeScript, JavaScript)

## **WEEK 2 - January 23rd**

* Finish the installation of docker dependencies and yarn on one machine 
* Send a mail (by the other group working on dashboard) to a git administrator for a problem of installation with Docker
* Correction of a config file problem on the git by the teacher and pull of the new version

## **WEEK 3 - January 30th**

* Installation and start of the FastAPI service on one machine using uvicorn in the oar docker container
* Continue to learn the techno
* Session of bug tracking on machines with docker's dependencies problems
* Exploration of all the files of the project in the git
  
## **WEEK 4 - February 6th**

* Reinstallation of docker on one machine where the problem of dependencies occurs
* Continue the exploration of the project's files and comprehension of his architecture

## **WEEK 5 - February 20th**

* Starting the migration from react-admin 3.2.3 to 4.6.2 using yarn and nvm dependencies
* Redaction of the installation file update
* Creation of [the mid-term presentation slide](./presentation_mi-parcours.pdf) of the 28th February.
* Participate to the github issue about docker-compose: https://github.com/oar-team/oar-docker-compose/issues/11

## **WEEK 6 - February 27th**

* Reading the different patches about react-admin
* Trying to install the environmnent on a machine from scratch
* Installing nvm to manage Node version in order to allow the upgrade of yarn and do the migration of react-admin

## **WEEK 7 - March 6th**

* Updating the node.js version to 14.0 int order to upgrade react-admin correctly.
* Fixing problems due to Babel with loader transcription system. Webpack can only read javascript files and the loaders can traduct files like css or typescript into javascript files.

## **WEEK 7 - March 7th**

* Cleaning git repository.
* Continue to debug the loader issue.
* Testing the installation tutorial on our git.

## **WEEK 8 - March 13th**

* Updating and fixing dependencies from yarn (fixing good versions for different modules)
* Debugging the loader problem. Webpack still can't build the dependencies tree due to bad loaders.
* Fixing a bug with a string allowedHost which should not be null. That leads to a non-allowed host who cannot launch the project.

## **WEEK 8 - March 14th**

* Fixing the issue with allowedHost (done).
* Advancing the migration on one branch and the version problems on another branch.
* Progressing on migration to 4.0.0 in order to continue to the 4.6.2.
  
## **WEEK 9 - March 20th**

* Completing the migration to the version 4.0.0 of React-Admin and making it operational.
* Adding the "Visual" part: it contains new features available with the migration. For example a toggle button that allows the user to change the theme of the application (dark or light). Also an AppBar that can be reduced in order to show the menu icons + title to navigate the different pages or just the icons. 
* Use of the React logo as a loading icon between pages.
* Changing the file `LoginPage.tsx` so he will do the job of the `LoginForm.tsx`. Cleaning useless code and merging (or commenting) the unnecessary files.
* Fixing the login internal server error. (done)
* Trying to understand the dataProvider problem with the function *updateMany* in the file `resource.tsx` and the *onClick* method.
  
## **WEEK 9 - March 21st**

* Use real logout button to see if you are connected.
* Trying to solve a display issue with Monika.
* Continue to understand the issue with `DataProvider.tsx`

## **WEEK 10 - March 27th**

* Testing our new implementation and functionalities.
* Redacting the final report.
* Fixing the issue with the display of Monika. (done)

## **WEEK 10 - March 28th**

* Cleaning and migrate the git repository on Gricad.
* Finishing the final report.
* Preparing the slides for the final presentation.