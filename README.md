# OAR-DASHBOARD 2022-2023

## **Team's members**:
* BONFILS Antoine
* FERRARI Julien
* CATTEAU Pierrick

## **Table of contents**

* [**Follow-up sheet - Team 17**](./oar-dashboard_info4_2022_2023.md)

* [**Project code (in my gitlab)**](https://gitlab.com/AntoineBF/projet-dashboard)

* [**Project code (gricad's link)**](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/22-23/17/projet-dashboard)

* [**Mid-term Presentation slide**](./presentation_mi-parcours.pdf)

* [**Final Report**](./rapport.pdf)

* [**Final Presentation slide**](./presentation.pdf)

* [**commits history**](./commits_history.html)

***
